const mongoose = require("mongoose");
const ContactModel = require("../models/contact.model");

const createContact = async (req, res) => {
  try {
    //B1: Thu thập
    const reqBody = req.body;
    //B2: Validate
    if (!reqBody.email) {
      return res.status(400).json({ message: "email is required" });
    }
    //B3: Xử lý
    let newContact = {
      email: reqBody.email,
    };
    let createdContact = await ContactModel.create(newContact);
    res.status(201).json({
      message: "Create Contact successfully",
      data: createdContact,
    });
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
    console.log(error.message);
  }
};

const getAllContacts = async (req, res) => {
  try {
    //B1: Thu thập
    //B2: Validate
    //B3: Xử lý
    let allContacts = await ContactModel.find();
    res.status(200).json({
      message: "Get All Contacts successfully",
      data: allContacts,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const getContactById = async (req, res) => {
  try {
    //B1: Thu thập
    const contactId = req.params.contactId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
      return res.status(400).json({
        message: "contactId is invalid",
      });
    }
    //B3: Xử lý
    let contactFound = await ContactModel.findById(contactId);
    if (!contactFound) {
      res.status(404).json({
        message: "Contact not found",
      });
    }
    res.status(200).json({
      message: "Find Contact By Id successfully",
      data: contactFound,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const updateContactById = async (req, res) => {
  try {
    //B1: Thu thập
    const contactId = req.params.contactId;
    const reqBody = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
      return res.status(400).json({
        message: "contactId is invalid",
      });
    }
    if (!reqBody.email && reqBody.email !== undefined) {
      return res.status(400).json({ message: "email is required" });
    }
    //B3: Xử lý
    let patchContact = {
      email: reqBody.email,
    };
    let updatedContact = await ContactModel.findByIdAndUpdate(
      contactId,
      patchContact,
      {
        new: true,
      }
    );
    if (!updatedContact) {
      res.status(404).json({
        message: "Contact not found",
      });
    }

    res.status(200).json({
      message: "Update Contact successfully",
      data: updatedContact,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const deleteContactById = async (req, res) => {
  try {
    //B1: Thu thập
    const contactId = req.params.contactId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
      return res.status(400).json({
        message: "contactId is invalid",
      });
    }
    //B3: Xử lý
    let deletedContact = await ContactModel.findByIdAndDelete(contactId);
    if (!deletedContact) {
      res.status(404).json({
        message: "Contact not found",
      });
    }
    res.status(200).json({
      message: "Delete Contact By Id successfully",
      data: deletedContact,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

module.exports = {
    createContact,
    getAllContacts,
    getContactById,
    updateContactById,
    deleteContactById
}