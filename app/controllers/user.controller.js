const mongoose = require("mongoose");
const UserModel = require("../models/user.model");

const createUser = async (req, res) => {
  try {
    //B1: Thu thập
    const reqBody = req.body;
    //B2: Validate
    if (!reqBody.fullName) {
      return res.status(400).json({ message: "fullName is required" });
    }
    if (!reqBody.phone) {
      return res.status(400).json({ message: "phone is required" });
    }
    //B3: Xử lý
    let newUser = {
      fullName: reqBody.fullName,
      phone: reqBody.phone,
      status: reqBody.status,
    };
    let createdUser = await UserModel.create(newUser);
    res.status(201).json({
      message: "Create User successfully",
      data: createdUser,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({ message: "phone is unique" });
    }
    res.status(500).json({ message: "Internal Server Error" });
    console.log(error.message);
  }
};

const getAllUsers = async (req, res) => {
  try {
    //B1: Thu thập
    const fullName = req.query.fullName;
    const phone = req.query.phone;
    //B2: Validate
    if (!fullName && fullName !== undefined) {
      return res.status(400).json({message: 'name is invalid'})
    }
    if (!phone && phone !== undefined) {
      return res.status(400).json({message: 'number is invalid'})
    }
    //B3: Xử lý
    //tạo bộ lọc
    const condition = {};
    if (fullName) {
      condition.fullName = fullName;
    }
    if (phone) {
      condition.phone = phone;
    }
    //tìm user hoặc all user
    let allUsers = await UserModel.find(condition);
    res.status(200).json({
      message: "Get All Users successfully",
      data: allUsers,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const getUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: "userId is invalid",
      });
    }
    //B3: Xử lý
    let userFound = await UserModel.findById(userId);
    if (!userFound) {
      res.status(404).json({
        message: "User not found",
      });
    }
    res.status(200).json({
      message: "Find User By Id successfully",
      data: userFound,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const updateUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    const reqBody = req.body;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: "userId is invalid",
      });
    }
    if (!reqBody.fullName && reqBody.fullName !== undefined) {
      return res.status(400).json({ message: "fullName is required" });
    }
    if (!reqBody.phone && reqBody.fullName !== undefined) {
      return res.status(400).json({ message: "phone is required" });
    }

    //B3: Xử lý
    let patchUser = {
      fullName: reqBody.fullName,
      phone: reqBody.phone,
      status: reqBody.status,
    };
    let updatedUser = await UserModel.findByIdAndUpdate(userId, patchUser, {
      new: true,
    });
    if (!updatedUser) {
      res.status(404).json({
        message: "User not found",
      });
    }

    res.status(200).json({
      message: "Update User successfully",
      data: updatedUser,
    });
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "phone is unique",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

const deleteUserById = async (req, res) => {
  try {
    //B1: Thu thập
    const userId = req.params.userId;
    //B2: Validate
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return res.status(400).json({
        message: "userId is invalid",
      });
    }
    //B3: Xử lý
    let deletedUser = await UserModel.findByIdAndDelete(userId);
    if (!deletedUser) {
      res.status(404).json({
        message: "User not found",
      });
    }
    res.status(200).json({
      message: "Delete User By Id successfully",
      data: deletedUser,
    });
  } catch (error) {
    res.status(500).json({
      message: "Internal Server Error",
    });
    console.log(error.message);
  }
};

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}
