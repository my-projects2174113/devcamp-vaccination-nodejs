const express = require('express');
const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require('../controllers/user.controller');

const UserRouter = express.Router();

UserRouter.post('/', createUser);

UserRouter.get('/', getAllUsers);

UserRouter.get('/:userId', getUserById);

UserRouter.put('/:userId', updateUserById);

UserRouter.delete('/:userId', deleteUserById);

module.exports = UserRouter;