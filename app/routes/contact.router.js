const express = require('express');
const {
    createContact,
    getAllContacts,
    getContactById,
    updateContactById,
    deleteContactById
} = require('../controllers/contact.controller');

const ContactRouter = express.Router();

ContactRouter.post('/', createContact);

ContactRouter.get('/', getAllContacts);

ContactRouter.get('/:contactId', getContactById);

ContactRouter.put('/:contactId', updateContactById);

ContactRouter.delete('/:contactId', deleteContactById);

module.exports = ContactRouter;