const logCurrentTimeMiddleWare = (req, res, next) => {
  const currentTime = new Date().toLocaleString();
  console.log("Current time:", currentTime);
  next();
};

module.exports = logCurrentTimeMiddleWare;