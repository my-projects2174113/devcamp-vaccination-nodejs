"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gContactId = 0;
const gBASE_URL = "http://localhost:8000/api/contacts";
const gHEADERS = { "Content-Type": "application/json;charset=UTF-8" };

// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCONTACT_COLS = ["stt", "email", "createdAt", "updatedAt", "action"];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCONTACT_STT_COL = 0;
const gCONTACT_EMAIL_COL = 1;
const gCONTACT_CREATED_AT_COL = 2;
const gCONTACT_UPDATED_AT_COL = 3;
const gCONTACT_ACTION_COL = 4;

// Khai báo DataTable & mapping collumns
var gContactTable = $("#contacts-table").DataTable({
  columns: [
    { data: gCONTACT_COLS[gCONTACT_STT_COL] },
    { data: gCONTACT_COLS[gCONTACT_EMAIL_COL] },
    { data: gCONTACT_COLS[gCONTACT_CREATED_AT_COL] },
    { data: gCONTACT_COLS[gCONTACT_UPDATED_AT_COL] },
    { data: gCONTACT_COLS[gCONTACT_ACTION_COL] },
  ],
  columnDefs: [
    {
      // định nghĩa lại cột STT
      targets: gCONTACT_STT_COL,
      render: (data, type, row, meta) => {
        return meta.row + 1;
      },
    },
    {
      // định nghĩa lại cột action
      targets: gCONTACT_ACTION_COL,
      defaultContent: `
        <i class="fas fa-edit text-primary btn update-contact"></i>
        <i class="fas fa-trash-alt text-danger btn delete-contact"></i>
        `,
      className: "text-center d-flex justify-content-around",
    },
  ],
});

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  // tải trang
  onPageLoading();

  // CRUD
  $("#btn-create-contact").click(() => {
    $("#create-contact-modal").modal("show");
  });
  $("#contacts-table").on("click", ".update-contact", function () {
    onBtnUpdateContactClick(this);
  });
  $("#contacts-table").on("click", ".delete-contact", function () {
    onBtnDeleteContactClick(this);
  });

  // confirm CRUD
  $("#btn-confirm-create-contact").click(onBtnConfirmCreateContactClick);
  $("#btn-confirm-update-contact").click(function () {
    onBtnConfirmUpdateContactClick(this);
  });
  $("#btn-confirm-delete-contact").click(function () {
    onBtnConfirmDeleteContactClick(this);
  });

  // modal
  $("#create-contact-modal").on("hidden.bs.modal", resetCreateForm);
  $("#update-contact-modal").on("hidden.bs.modal", resetUpdateForm);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm thực thi khi trang được load
function onPageLoading() {
  getAllContacts();
}

//CRUD
async function onBtnUpdateContactClick(paramButton) {
  //B1: Thu thập
  gContactId = collectContactId(paramButton);
  //B2: Validate
  //B3: Xử lý
  try {
    const response = await callAPIGetContactById();
    const result = await response.json();
    handleGetContactByIdSuccess(result);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

function onBtnDeleteContactClick(paramButton) {
  gContactId = collectContactId(paramButton);
  $("#delete-contact-modal").modal("show");
}

// confirm CRUD
async function onBtnConfirmCreateContactClick() {
  const newContact = {
    email: "",
  };
  //B1: Thu thập
  collectCreateContact(newContact);
  //B2: Validate
  let vCheck = validateContact(newContact);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPICreateContact(newContact);
      const result = await response.json();
      handleCreateContactSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

async function onBtnConfirmUpdateContactClick() {
  const patchContact = {
    email: "",
  };
  //B1: Thu thập
  collectUpdateContact(patchContact);
  //B2: Validate
  let vCheck = validateContact(patchContact);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPIUpdateContact(patchContact);
      const result = await response.json();
      handleUpdateContactSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

async function onBtnConfirmDeleteContactClick() {
  //B1: Thu thập
  //B2: Validate
  //B3: Xử lý
  try {
    const response = await callAPIDeleteContact();
    const result = await response.json();
    handleDeleteContactSuccess(result);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
async function getAllContacts() {
  try {
    const response = await callAPIGetAllContacts();
    const result = await response.json();
    loadDataToTable(result.data);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

//API
async function callAPIGetAllContacts() {
  return await fetch(gBASE_URL, {
    method: "get",
    mode: "cors",
    headers: gHEADERS,
  });
}

async function callAPICreateContact(paramContact) {
  return await fetch(gBASE_URL, {
    method: "post",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramContact),
  });
}

async function callAPIGetContactById() {
  return await fetch(gBASE_URL + "/" + gContactId, {
    method: "get",
    mode: "cors",
    headers: gHEADERS,
  });
}

async function callAPIUpdateContact(paramContact) {
  return await fetch(gBASE_URL + "/" + gContactId, {
    method: "put",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramContact),
  });
}

async function callAPIDeleteContact() {
  return await fetch(gBASE_URL + "/" + gContactId, {
    method: "delete",
    mode: "cors",
    headers: gHEADERS,
  });
}

//Collect Data
function collectContactId(paramButton) {
  var vTableRow = $(paramButton).closest("tr");
  var vContactRowData = gContactTable.row(vTableRow).data();
  return vContactRowData._id;
}

function collectCreateContact(paramContact) {
  paramContact.email = $.trim($("#inp-email-create").val());
}

function collectUpdateContact(paramContact) {
  paramContact.email = $.trim($("#inp-email-update").val());
}

//validate Data
function validateContact(paramContact) {
  if (!validateEmail(paramContact.email)) {
    alert("Email is invalid");
    return false;
  }
  return true;
}

const validateEmail = (email) => {
  const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return email.match(emailFormat) ? true : false;
};

//Hàm dùng chung
function loadDataToTable(paramContactArr) {
  gContactTable.clear();
  gContactTable.rows.add(paramContactArr);
  gContactTable.draw();
}

function handleCreateContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#create-contact-modal").modal("hide");
  getAllContacts();
}

function handleGetContactByIdSuccess(paramResult) {
  $("#update-contact-modal").modal("show");
  const data = paramResult.data;
  $("#inp-_id-update").val(data._id);
  $("#inp-email-update").val(data.email);
}

function handleUpdateContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#update-contact-modal").modal("hide");
  getAllContacts();
}

function handleDeleteContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#delete-contact-modal").modal("hide");
  getAllContacts();
}

function resetCreateForm() {
  $("#inp-email-create").val("");
}

function resetUpdateForm() {
  $("#inp-_id-update").val("");
  $("#inp-email-update").val("");
}
