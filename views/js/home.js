"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://localhost:8000";
const gHEADERS = { "Content-Type": "application/json" };

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  // Chọn phần tử HTML có id là "inp-phone-number"
  const phoneInputField = document.querySelector("#inp-phone-number");
  // Sử dụng thư viện intlTelInput để tạo trường nhập số điện thoại quốc tế
  const phoneInput = window.intlTelInput(phoneInputField, {
    // Quốc gia ban đầu sẽ được tự động xác định
    initialCountry: "auto",
    // Hàm để tìm quốc gia dựa trên địa chỉ IP của người dùng
    geoIpLookup: getIp,
    // Đường dẫn đến tập lệnh tiện ích cho thư viện intlTelInput
    utilsScript:
      "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
  });

  //focus form submit
  $("#btn-get-vaccine").click(() => {
    $("#inp-patient-fullname").focus();
  });
  //focus send email
  $("#btn-help-centre").click(() => {
    $("#inp-email").focus();
  });
  //check result
  $("#btn-results-check").click(onBtnCheckResult);
  //verify phone number
  $("#btn-verify-phone").click(onBtnVerifyPhone);
  //submit user
  $("#btn-submit").click(onBtnSubmitUser);
  //send email
  $("#btn-send-email").click(onBtnSendEmail);
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//check result
async function onBtnCheckResult() {
  event.preventDefault();
  const checkUser = {
    fullName: "",
    phone: "",
  };
  //B1: Thu thập
  collectCheckUser(checkUser);
  //B2: Validate
  let vCheck = validateUser(checkUser);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPICheckUser(checkUser);
      const result = await response.json();
      // Xử lý người dùng sau khi tạo thành công
      handleCheckUserSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

//verify phone func
function onBtnVerifyPhone() {
  event.preventDefault();
  //B1: Thu thập
  let inpPhone = $.trim($("#inp-phone-number").val());
  //B2: Validate
  if (!validatePhone(inpPhone)) {
    alert("Number phone is invalid");
    return;
  }
  //B3: Xử lý
  alert("Number phone is valid");
}

//submit user
async function onBtnSubmitUser() {
  event.preventDefault();
  const newUser = {
    fullName: "",
    phone: "",
  };
  //B1: Thu thập
  collectCreateUser(newUser);
  //B2: Validate
  let vCheck = validateUser(newUser);
  if (vCheck) {
    //B3: Xử lý
    try {
      const response = await callAPICreateUser(newUser);
      const result = await response.json();
      // Xử lý người dùng sau khi tạo thành công
      handleCreateUserSuccess(result);
    } catch (error) {
      alert("An error occurred, please try again.");
    }
  }
}

//send email
async function onBtnSendEmail() {
  const newContact = {
    email: "",
  };
  //B1: Thu thập
  newContact.email = $.trim($("#inp-email").val());
  //B2: Validate
  if (!validateEmail(newContact.email)) {
    alert("Email is invalid");
    return;
  }
  //B3: Xử lý
  try {
    const response = await callAPICreateContact(newContact);
    const result = await response.json();
    // Xử lý hiển thị sau khi tạo thành công
    handleCreateContactSuccess(result);
  } catch (error) {
    alert("An error occurred, please try again.");
  }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm để lấy thông tin quốc gia dựa trên địa chỉ IP của người dùng
function getIp(callback) {
  // Gửi yêu cầu đến API để lấy thông tin địa chỉ IP
  fetch("https://ipinfo.io/json?token=ef64ed6d5580fc", {
    headers: { Accept: "application/json" },
  })
    .then((resp) => resp.json())
    .catch(() => {
      // Trong trường hợp không thể lấy được thông tin địa chỉ IP, trả về quốc gia mặc định
      return {
        country: "us",
      };
    })
    .then((resp) => callback(resp.country)); // Gọi hàm callback với mã quốc gia nhận được
}

//call API CheckUser
async function callAPICheckUser(paramUser) {
  console.log();
  return await fetch(
    gBASE_URL +
      "/api/users/" +
      `?fullName=${paramUser.fullName}&phone=${paramUser.phone}`,
    {
      method: "get",
      mode: "cors",
      headers: gHEADERS,
    }
  );
}

//call API CreateUser
async function callAPICreateUser(paramUser) {
  return await fetch(gBASE_URL + "/api/users", {
    method: "post",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramUser),
  });
}

//call API CreateContact
async function callAPICreateContact(paramContact) {
  return await fetch(gBASE_URL + "/api/contacts", {
    method: "post",
    mode: "cors",
    headers: gHEADERS,
    body: JSON.stringify(paramContact),
  });
}

//collect CheckUser
function collectCheckUser(paramUser) {
  paramUser.fullName = $.trim($("#inp-results-name").val());
  paramUser.phone = $.trim($("#inp-results-number").val());
}

//collect CreateUser
function collectCreateUser(paramUser) {
  paramUser.fullName = $.trim($("#inp-patient-fullname").val());
  paramUser.phone = $.trim($("#inp-phone-number").val());
}

//validate CreateUser
function validateUser(paramUser) {
  if (!paramUser.fullName) {
    alert("Fullname is invalid");
    return false;
  }
  if (!validatePhone(paramUser.phone)) {
    alert("Number phone is invalid");
    return false;
  }
  return true;
}

//validate email func
const validateEmail = (email) => {
  const emailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return email.match(emailFormat) ? true : false;
};

//validate phone func
const validatePhone = (phone) => {
  const phoneFormat = /(84|0[3|5|7|8|9])+([0-9]{8})\b/g;
  return phone.match(phoneFormat) ? true : false;
};

// Xử lý kết quả sau khi lấy thành công
function handleCheckUserSuccess(paramResult) {
  if (paramResult.data.length < 1) {
    alert("You are not yet in the COVID-19 vaccine check list.");
    return;
  } else {
    alert(`Your result: ${paramResult.data[0].status}`);
  }
}

// Xử lý người dùng sau khi tạo thành công
function handleCreateUserSuccess(paramResult) {
  alert(paramResult.message);
  $("#inp-patient-fullname").val("");
  $("#inp-phone-number").val("");
}

// Xử lý người dùng sau khi tạo thành công
function handleCreateContactSuccess(paramResult) {
  alert(paramResult.message);
  $("#inp-email").val("");
}
